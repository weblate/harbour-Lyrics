# Lyrics

A music lyrics application for [Sailfish OS](https://sailfishos.org).

Currently it supports:
  * [ChartLyrics](http://chartlyrics.com)
  * [Genius](http://genius.com)
  * [LyricsMania](http://www.lyricsmania.com)

[![Build Status](https://gitlab.com/ilpianista/harbour-Lyrics/badges/master/pipeline.svg)](https://gitlab.com/ilpianista/harbour-Lyrics/pipelines)

## Translations

[![Translation status](https://hosted.weblate.org/widgets/harbour-lyrics/-/svg-badge.svg)](https://hosted.weblate.org/engage/harbour-lyrics/?utm_source=widget)

Translations via [Weblate](https://hosted.weblate.org/projects/harbour-lyrics/), thank you!

## Donate

Donations via [Liberapay](https://liberapay.com/ilpianista) or Bitcoin (1Ph3hFEoQaD4PK6MhL3kBNNh9FZFBfisEH) are always welcomed, _thank you_!

## License

MIT
