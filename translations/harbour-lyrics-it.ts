<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="it">
<context>
    <name>MainPage</name>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="76"/>
        <source>Clear</source>
        <translation>Pulisci</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="70"/>
        <source>Settings</source>
        <translation>Impostazioni</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="180"/>
        <source>Powered by %1</source>
        <translation>Stai usando %1</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="120"/>
        <source>Artist</source>
        <translation>Artista</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="89"/>
        <source>Copy to clipboard</source>
        <translation>Copia negli appunti</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="132"/>
        <source>Song</source>
        <translation>Brano</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="149"/>
        <source>Search</source>
        <translation>Cerca</translation>
    </message>
</context>
<context>
    <name>Settings</name>
    <message>
        <location filename="../qml/pages/Settings.qml" line="39"/>
        <source>Settings</source>
        <translation>Impostazioni</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="45"/>
        <source>Provider</source>
        <translation>Servizio</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="66"/>
        <source>Enable Media Player scanner</source>
        <translation>Abilita riconoscimento automatico</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="67"/>
        <source>Checks Media Player to get song info. Do not focus any field to allow text substitution.</source>
        <translation>Ottiene le informazioni del brano dal lettore multimediale. Non posizionare il cursore in nessun campo di testo in modo da permettere la compilazione automatica.</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="76"/>
        <source>Clear cache</source>
        <translation>Pulisci cache</translation>
    </message>
</context>
</TS>
